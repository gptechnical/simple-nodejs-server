testService = require './TestService'

module.exports = (app)->

	app.get '/ping1', (req, res)=>
		res.send testService.ping1()

	app.get '/ping2', (req, res)=>
		res.send testService.ping2()
