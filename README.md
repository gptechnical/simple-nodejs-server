Simple NodeJS Server
====================

An bare-bones express (nodejs) server featuring:

* A tidy Gruntfile
* Winston Logging
* DotEnv environment variables
* TestRoutes  x 2
* TestService


